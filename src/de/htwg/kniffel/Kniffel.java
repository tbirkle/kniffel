package de.htwg.kniffel;

import org.apache.log4j.PropertyConfigurator;

import de.htwg.kniffel.controller.IKniffelController;
import de.htwg.kniffel.controller.impl.KniffelController;
import de.htwg.kniffel.model.impl.GridFactory;
import de.htwg.kniffel.view.gui.KniffelFrame;
import de.htwg.kniffel.view.tui.TextUI;


public final class Kniffel {
	/**
	 * private constructor
	 */
	private Kniffel() {
		
	}
	/**
	 * main method starts the game
	 * @param args
	 */
	public static void main(String[] args) {
		// Set up logging through log4j
		PropertyConfigurator.configure("log4j.properties");
		IKniffelController controller;
		
		if(args.length != 0 && args[0].equals("-d")) {
			controller = new de.htwg.kniffel.controller.logwrapper.KniffelController(new GridFactory());
		} else {
			controller = new KniffelController(new GridFactory());
		}
				
		new KniffelFrame(controller);
		TextUI tui = new TextUI(controller);
		boolean quit = false;

		while(!quit) {
			quit = tui.iterate();
		}
	}
}
