package de.htwg.kniffel.controller;

import de.htwg.kniffel.model.IDice;
import de.htwg.kniffel.model.IPlayer;
import de.htwg.kniffel.observer.IObservable;



public interface IKniffelController extends IObservable {

	int getDiceTry();
	
	IPlayer getWinner();
	
	void initializePlayerNames();
	String addPlayer(String name);
	/**
	 * 
	 * @return true if all players finished
	 */
	boolean checkFinishAllPlayers();


	/**
	 * sets the score
	 * @param scoreNumber
	 * @return returns true if the score is set
	 */
	boolean setScore(int scoreNumber);


	/**
	 * calculates the available dice and rolls
	 * @return 0
	 */
	int roll();

	/**
	 * returns an array of all dice
	 * @return Dice[]
	 */
	IDice[] getDice();

	/**
	 * saves the dice of the player
	 * @param numberDice
	 */
	void saveDice(String numberDice);

	/**
	 * removes the dice which the player saved
	 * @param numberDice
	 */
	int removeDice(String numberDice);

	/**
	 * draws the scoreboard
	 * @return String
	 */
	String drawScoreboard();

	/**
	 * 
	 * @return the actual player
	 */
	IPlayer getPlayer();

	/**
	 * returns the player who won the game
	 * @return Player
	 */
	IPlayer checkWinner();

	
	IPlayer[] getPlayers();
}