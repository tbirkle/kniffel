package de.htwg.kniffel.controller.impl;


import java.util.LinkedList;
import java.util.List;
import de.htwg.kniffel.controller.IKniffelController;
import de.htwg.kniffel.model.IDice;
import de.htwg.kniffel.model.IGrid;
import de.htwg.kniffel.model.IGridFactory;
import de.htwg.kniffel.model.IPlayer;
import de.htwg.kniffel.model.IScore;
import de.htwg.kniffel.model.impl.Dice;
import de.htwg.kniffel.model.impl.Player;
import de.htwg.kniffel.observer.Observable;
import de.htwg.kniffel.util.YahtzeeConstants;



public class KniffelController extends Observable implements YahtzeeConstants, IKniffelController {

	private static final int FIVE_IN_A_LINE = 5;
	private static final int FOUR_IN_A_LINE = 4;
	private static final int FIVE_A_KIND = 5;
	private static final int FOUR_A_KIND = 4;
	private static final int THREE_A_KIND = 3;
	private static final int TWO_A_KIND = 2;
	private static final int MAX_EYES = 6;
	
	private IGridFactory gridFactory;
	private IGrid grid;
	private IDice[] dice;
	private IPlayer players[];
	private IPlayer actualPlayer;
	private int playerNumber = 0;
	private List<String> playerNames;
	private int diceTry;
	private IPlayer winner;

	
	
	public KniffelController(IGridFactory gridFactory) {
		diceTry = 0;
		this.gridFactory = gridFactory;
		playerNames = new LinkedList<String>();
		initializeDice();
	}

	
	private void initializeDice() {
		this.dice = new Dice[NUMBER_DICE];
		for (int i = 0; i < this.dice.length; i++) {
			dice[i] = new Dice(MAX_EYES);
		}
	}
	
	@Override
	public int getDiceTry() {
		return this.diceTry;
	}
	
	private void resetDice() {
		for (IDice dice : this.dice) {
			dice.setIsSaved(false);
			dice.setResult(0);
		}
	}
	
	public String addPlayer(String name) {
		playerNames.add(name);
		return name;
	}
	
	public void initializePlayerNames() {
		players = new IPlayer[playerNames.size()];
		for (int i = 0; i < playerNames.size(); i++) {
			this.players[i] = new Player(playerNames.get(i), i);
		}
		actualPlayer = players[0];
		grid = gridFactory.create("Kniffel", players);
	}
	
	public IScore getScore() {
		return actualPlayer.getScore();
	}
	
	public IPlayer[] getPlayers() {
		return players;
	}
	
	public void setTestDice(int index, int number) {
		dice[index].setResult(number);
	}

	/**
	 * rolls all dice
	 */
	private void rollDice() {
		for (IDice d : dice) {
			d.rollDice();
		}
	}
	
	/**
	 * iterates the players
	 * @return the number of the actualPlayer
	 */
	public int iteratePlayers() {
		while(!checkFinishAllPlayers()) {
			playerNumber++;
			if(playerNumber >= grid.getPlayers().length) {
				playerNumber = 0;
			}
			this.actualPlayer = grid.getPlayers()[playerNumber];
			if(!this.actualPlayer.checkFinish()) {
				break;
			}
		}
		return playerNumber;
	}
	
	/* (non-Javadoc)
	 * @see de.htwg.kniffel.controller.IKniffelController#setScore(int)
	 */
	@Override
	public boolean setScore(int scoreNumber) {
		int scoreValue = checkPossibleMove(scoreNumber);
		boolean retVal = false;
		retVal = actualPlayer.setScore(scoreNumber, scoreValue);
		if(retVal) {
			if(actualPlayer.checkFinish()) {
				actualPlayer.setFinished(true);
			}
			diceTry = 0;
			iteratePlayers();
			notifyObservers();
			resetDice();
			
		}
		return retVal;
	}
	
	/**
	 * Returns the value of the score to set
	 * @param ScoreNumber
	 * @return int
	 */
	private int checkPossibleMove(int scoreNumber) {
		int scoreValue = 0;
		if(scoreNumber < BONUS) {
			scoreValue = checkTopScore(scoreNumber);
		}
		if(scoreNumber == FOUR_OF_A_KIND || scoreNumber == THREE_OF_A_KIND || scoreNumber == YAHTZEE) {
			scoreValue = setXOfAKind(scoreNumber);
		}
		if(scoreNumber == CHANCE) {
			scoreValue = countResult();
		}
		if(scoreNumber == SMALL_STRAIGHT) {
			scoreValue = setSmallStraight(scoreNumber);
		}
		if(scoreNumber == LARGE_STRAIGHT) {
			scoreValue = setLargeStraight(scoreNumber);
		}
		if(scoreNumber == FULL_HOUSE) {
			scoreValue = setFullHouse();
		}
		return scoreValue;
	}
	
	/**
	 * sets the value of full house
	 * @param scoreNumber
	 * @return the value of the score
	 */
	private int setFullHouse() {
		int scoreValue = 0;
		if(checkFullHouse()) {
			scoreValue = FULL_HOUSE_SCORE;
		}
		return scoreValue;
	}
	
	/**
	 * sets the value of de.htwg.kniffel.yahtzee, three of a kind and four of a kind
	 * @param scoreNumber
	 * @return the value of the score
	 */
	private int setXOfAKind(int scoreNumber) {
		int scoreValue = 0;
		if(checkEqualDice(scoreNumber)) {
			if(scoreNumber == YAHTZEE) {
				scoreValue = YAHTZEE_SCORE;
			} else {
				scoreValue = countResult();
			}
		}
		return scoreValue;
	}
	
	/**
	 * sets the value of a small straight
	 * @param scoreNumber
	 * @return the value of the score
	 */
	private int setSmallStraight (int scoreNumber) {
		int scoreValue = 0;
		if(checkStraight(scoreNumber)) {
			scoreValue = SMALL_STRAIGHT_SCORE;
		}
		return scoreValue;
	}
	/**
	 * sets the value of a large straight
	 * @param scoreNumber
	 * @return the value of the score
	 */
	private int setLargeStraight (int scoreNumber) {
		int scoreValue = 0;
		if(checkStraight(scoreNumber)) {
			scoreValue = LARGE_STRAIGHT_SCORE;
		}
		return scoreValue;
	}
	
	/**
	 * 
	 * @return returns true if a full house can be set
	 */
	private boolean checkFullHouse() {

		List<Integer> tmpDice = createSortedList();
		boolean found3 = false;
		boolean found2 = false;
		int count = 1;
		for (int i = 1; i < tmpDice.size(); i++) {
			if(tmpDice.get(i) == tmpDice.get(i-1)) {
				count++;
			} else {
				if(count == THREE_A_KIND) {
					found3 = true;
				} else if (count == TWO_A_KIND) {
					found2 = true;
				}
				count = 1;
			}
		}
		
		if(count == THREE_A_KIND) {
			found3 = true;
		} else if(count == TWO_A_KIND) {
			found2 = true;
		}
		
		if(found2 && found3) {
			return true;
		}
		
		return false;
	}
	
	/**
	 * 
	 * @param scoreNumber
	 * @return true if a small or large straight can be set
	 */
	private boolean checkStraight(int scoreNumber) {
		List<Integer> tmpDice = createSortedList();
		int tmp = 0;
		if(scoreNumber == LARGE_STRAIGHT) {
			tmp = FIVE_IN_A_LINE;
		}
		if(scoreNumber == SMALL_STRAIGHT) {
			tmp = FOUR_IN_A_LINE;
		}
		
		int line = 1;
		for (int i = 1; i < tmpDice.size(); i++) {
			if(tmpDice.get(i-1)+1 == tmpDice.get(i)) {
				line++;
			}
		}
		
		if(line >= tmp) {
			return true;
		}
		
		return false;
	}
	
	/**
	 * counts the result of all dice
	 * @return int
	 */
	private int countResult() {
		int scoreValue = 0;
		
		for(IDice d : dice) {
			scoreValue += d.getResult();
		}
		return scoreValue;
	}
	
	/**
	 * 
	 * @return a sorted list of the type List<Integer>
	 */
	private List<Integer> createSortedList() {
		List<Integer> tmpList = new LinkedList<Integer>();
		for(IDice d : dice) {
			if(d.getResult() != 0) {
				tmpList.add(d.getResult());
			}
		}
		java.util.Collections.sort(tmpList);
		
		
		return tmpList;
	}
	
	/**
	 * checks if there are equal dice in the pool
	 * @param scoreNumber
	 * @return returns true if they are equal
	 */
	private boolean checkEqualDice(int scoreNumber) {
		List<Integer> tmpDice = createSortedList();
		int numberOfEqualDice = 0;
		int count = 1;
		if(scoreNumber == THREE_OF_A_KIND) {
			numberOfEqualDice = THREE_A_KIND;
		}
		if(scoreNumber == FOUR_OF_A_KIND) {
			numberOfEqualDice = FOUR_A_KIND;
		}
		if(scoreNumber == YAHTZEE) {
			numberOfEqualDice = FIVE_A_KIND;
		}
		for (int i = 1; i < tmpDice.size(); i++) {
			
			if(tmpDice.get(i) == tmpDice.get(i-1)) {
				count++;
			} else {
				if(count < numberOfEqualDice) {
					count = 1;
				}
			}
		}
		
		if(count >= numberOfEqualDice) {
			return true;
		}
		
		return false;
	}
	/**
	 * 
	 * @param scoreNumber
	 * @return int
	 */
	private int checkTopScore(int scoreNumber) {
		int scoreValue = 0;
		for(IDice d : dice) {
			if(d.getResult() == scoreNumber) {
				scoreValue += scoreNumber;
			}
		}
		return scoreValue;
	}
	
	/* (non-Javadoc)
	 * @see de.htwg.kniffel.controller.IKniffelController#roll()
	 */
	@Override
	public int roll() {
		if(diceTry < MAX_TRYS) {
			rollDice();
			diceTry++;
		}
		return diceTry;
	}
	
	/* (non-Javadoc)
	 * @see de.htwg.kniffel.controller.IKniffelController#getDice()
	 */
	@Override
	public IDice[] getDice() {
		return dice;
	}
	
	/* (non-Javadoc)
	 * @see de.htwg.kniffel.controller.IKniffelController#setDice(java.lang.String)
	 */
	@Override
	public void saveDice(String numberDice) {
		int number = Integer.parseInt(numberDice);
		this.dice[number].setIsSaved(true);
	}
	
	/* (non-Javadoc)
	 * @see de.htwg.kniffel.controller.IKniffelController#removeDice(java.lang.String)
	 */
	@Override
	public int removeDice(String numberDice) {
		int number = Integer.parseInt(numberDice);
		this.dice[number].setIsSaved(false);
		return number;
	}
	
	/* (non-Javadoc)
	 * @see de.htwg.kniffel.controller.IKniffelController#drawScoreboard()
	 */
	@Override
	public String drawScoreboard() {
		return grid.toString();
	}
	
	/* (non-Javadoc)
	 * @see de.htwg.kniffel.controller.IKniffelController#getPlayer()
	 */
	@Override
	public IPlayer getPlayer() {
		return this.actualPlayer;
	}
	
	/**
	 * 
	 * @return true if all players finished
	 */
	@Override
	public boolean checkFinishAllPlayers() {
		boolean finished = false;
		for (int i = 0; i < players.length; i++) {
			finished = players[i].checkFinish();
			if(!finished) {
				return false;
			}
		}
		winner = checkWinner();
		return true;
	}
	
	/* (non-Javadoc)
	 * @see de.htwg.kniffel.controller.IKniffelController#checkWinner()
	 */
	@Override
	public IPlayer checkWinner() {
		IPlayer winner = null;
		int score = 0;
		for (int i = 0; i < players.length; i++) {
			if(score < players[i].getScore().getPlayerScore()) {
				score = players[i].getScore().getPlayerScore();
				winner = players[i];
			}
		}
		return winner;
	}
	
	public IPlayer getWinner() {
		return winner;
	}
}
