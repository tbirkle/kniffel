package de.htwg.kniffel.observer;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * class of the observables
 *
 */
public class Observable implements IObservable {
	private List<IObserver> subscribers = new ArrayList<IObserver>(2);
	/* (non-Javadoc)
	 * @see de.htwg.kniffel.observer.IObservable#addObserver(de.htwg.kniffel.observer.IObserver)
	 */
	@Override
	public void addObserver(IObserver s) {
		subscribers.add(s);
	}
	/* (non-Javadoc)
	 * @see de.htwg.kniffel.observer.IObservable#removeObserver(de.htwg.kniffel.observer.IObserver)
	 */
	@Override
	public void removeObserver(IObserver s) {
		subscribers.remove(s);
	}

	/* (non-Javadoc)
	 * @see de.htwg.kniffel.observer.IObservable#removeAllObservers()
	 */
	@Override
	public void removeAllObservers() {
		subscribers.clear();
	}

	/* (non-Javadoc)
	 * @see de.htwg.kniffel.observer.IObservable#notifyObservers()
	 */
	@Override
	public void notifyObservers() {
		for ( Iterator<IObserver> iter = subscribers.iterator(); iter.hasNext();) {
			IObserver observer = iter.next();
			observer.update();
		}
	}
}
