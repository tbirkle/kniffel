package de.htwg.kniffel.observer;

public interface IObservable {

	/**
	 * adds the de.htwg.kniffel.observer s to the list
	 * @param s
	 */
	void addObserver(IObserver s);

	/**
	 * removes the de.htwg.kniffel.observer s from the list
	 * @param s
	 */
	void removeObserver(IObserver s);

	/**
	 * removes all oberservers of the list
	 */
	void removeAllObservers();

	/**
	 * notifies the observers
	 */
	void notifyObservers();

}