package de.htwg.kniffel.observer;

/**
 * 
 * Interface of the Observer pattern
 */
public interface IObserver {
	/**
	 * update() is called if the observers are notified
	 */
	void update();
}
