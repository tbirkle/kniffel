package de.htwg.kniffel.util;

public final class KniffelTextConstants implements YahtzeeConstants {
	
	private KniffelTextConstants() {
		
	}
	
	private static String getTopText(int scoreNumber) {
		if(scoreNumber == ONES) {
			return "Ones";
		}
		if(scoreNumber == TWOS) {
			return "Twos";
		}
		if(scoreNumber == THREES) {
			return "Threes";
		}
		if(scoreNumber == FOURS) {
			return "Fours";
		}
		if(scoreNumber == FIVES) {
			return "Fives";
		}
		if(scoreNumber == SIX) {
			return "Six";
		}
		if(scoreNumber == BONUS) {
			return "Bonus";
		}
		return "Test";
	}
	
	private static String getBottomText(int scoreNumber) {
		if(scoreNumber == FULL_HOUSE) {
		return "Full House";
		}
		if(scoreNumber == THREE_OF_A_KIND) {
			return "Three of a Kind";
		}
		if(scoreNumber == FOUR_OF_A_KIND) {
			return "Four of a Kind";
		}
		if(scoreNumber == YAHTZEE) {
			return "Kniffel";
		}
		if(scoreNumber == CHANCE) {
			return "Chance";
		}
		if(scoreNumber == SMALL_STRAIGHT) {
			return "Small Straight";
		}
		if(scoreNumber == LARGE_STRAIGHT) {
			return "Large Straight";
		}
		return "Test";
	}
	
	private static String getScoreBoardText(int scoreNumber) {
		if(scoreNumber == TOP_SUM) {
			return "Top Sum";
		}
		if(scoreNumber == BOTTOM_SUM) {
			return "Bottom Sum";
		}
		if(scoreNumber == TOTAL_SUM) {
			return "Total Sum";
		}
		return "Test";
	}
	
	public static String getText(int scoreNumber) {
		
		if(scoreNumber < THREE_OF_A_KIND) {
			return getTopText(scoreNumber);
		}
		if(scoreNumber < TOP_SUM) {
			return getBottomText(scoreNumber);
		}
		if(scoreNumber <= TOTAL_SUM) {
			return getScoreBoardText(scoreNumber);
		}
		return "Test";
	}
}
