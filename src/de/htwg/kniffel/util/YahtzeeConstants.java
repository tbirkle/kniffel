package de.htwg.kniffel.util;

public interface YahtzeeConstants {
	
	
	int MAX_TRYS = 3;
	
	/**
	 * maximum number of players
	 */
	
	int MAX_PLAYERS = 4;
	/**
	 * maximum number of dice
	 */
	int NUMBER_DICE = 5;
	
	/**
	 * Points of the bonus score on the top of the scoreboard
	 */
	int BONUS_SCORE = 35;
	
	/**
	 * Points of a Full House
	 */
	int FULL_HOUSE_SCORE = 25;
	
	/**
	 * Points of a Small Straight
	 */
	int SMALL_STRAIGHT_SCORE = 30;
	
	/**
	 * Points of a Large Straight
	 */
	int LARGE_STRAIGHT_SCORE = 40;
	
	/**
	 * Points of a Kniffel
	 */
	int YAHTZEE_SCORE = 50;
	
	/**
	 * Points to score to get the bonus points
	 */
	int BONUS_LIMIT = 63;
	
	
	/**
	 * The number of the scores on the Scoreboard
	 */
	int ONES = 1;
	int TWOS = 2;
	int THREES = 3;
	int FOURS = 4;
	int FIVES = 5;
	int SIX = 6;
	int BONUS = 7;
	int THREE_OF_A_KIND = 8;
	int FOUR_OF_A_KIND = 9;
	int FULL_HOUSE = 10;
	int SMALL_STRAIGHT = 11;
	int LARGE_STRAIGHT = 12;
	int YAHTZEE = 13;
	int CHANCE = 14;
	
	int TOP_SUM = 15;
	int BOTTOM_SUM = 16;
	int TOTAL_SUM = 17;
	
}