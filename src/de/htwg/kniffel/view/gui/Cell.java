package de.htwg.kniffel.view.gui;


import java.awt.GridLayout;
import javax.swing.BorderFactory;
import javax.swing.JLabel;
import de.htwg.kniffel.util.YahtzeeConstants;


public class Cell extends JLabel implements YahtzeeConstants {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3088460312043561400L;
	
	
	public Cell(String value) {
		this.setLayout(new GridLayout(1,1));
		setText(value);
		setBorder(BorderFactory.createLoweredBevelBorder());
	}
}
