package de.htwg.kniffel.view.gui;


import java.awt.Graphics;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.ImageIcon;
import javax.swing.JToggleButton;

import de.htwg.kniffel.controller.IKniffelController;
import de.htwg.kniffel.model.IDice;
import de.htwg.kniffel.util.YahtzeeConstants;

public class DiceButton extends JToggleButton implements YahtzeeConstants{
	
	private static final long serialVersionUID = 1L;
	private IDice[] dice;
	private int diceNumber;
	
	public DiceButton (final IKniffelController controller,final int number) {
		diceNumber = number;
		dice = controller.getDice();
		saveDiceIcon();
		this.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent e) {

				if(getState()) {
					controller.removeDice(String.valueOf(number));
				} else {
					controller.saveDice(String.valueOf(number));
				}
			}
		});	
	}
	
	private boolean getState() {
		return this.isSelected();
	}
	
	private void saveDiceIcon() {
		int value = dice[diceNumber].getResult();
		switch(value) {
		case 0:
			setIcon(new ImageIcon("icons/EMPTY.gif"));
			this.setSelected(false);
			break;
		case ONES:
			setIcon(new ImageIcon("icons/ONE.gif"));
			break;
		case TWOS:
			setIcon(new ImageIcon("icons/TWO.gif"));
			break;
		case THREES:
			setIcon(new ImageIcon("icons/THREE.gif"));
			break;
		case FOURS:
			setIcon(new ImageIcon("icons/FOUR.gif"));
			break;
		case FIVES:
			setIcon(new ImageIcon("icons/FIVE.gif"));
			break;
		case SIX:
			setIcon(new ImageIcon("icons/SIX.gif"));
			break;	
		}
	}
	
	public void setIcons() {
		saveDiceIcon();
	}

	
	@Override
	public void paint(Graphics g) {
		super.paint(g);
		setIcons();
	}
}
