package de.htwg.kniffel.view.gui;

import java.awt.GridLayout;

import javax.swing.JPanel;
import de.htwg.kniffel.controller.IKniffelController;
import de.htwg.kniffel.util.YahtzeeConstants;

public class DicePanel extends JPanel implements YahtzeeConstants {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5770974282376684627L;
	private DiceButtonPanel dice;
	private ThrowDiceButton button;
	
	public DicePanel(IKniffelController controller) {
		dice = new DiceButtonPanel(controller);
		setLayout(new GridLayout(1, 2));
		button = new ThrowDiceButton(controller, dice);
		add(dice);
		add(button);
	}
	
}
