package de.htwg.kniffel.view.gui;

import java.awt.Graphics;
import java.awt.GridLayout;

import javax.swing.JPanel;

import de.htwg.kniffel.controller.IKniffelController;
import de.htwg.kniffel.model.IPlayer;
import de.htwg.kniffel.util.YahtzeeConstants;

public class PlayerPanel extends JPanel implements YahtzeeConstants {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1277709507213600359L;
	private static final int MAX_ROWS = 18;
	private static final int MAX_COLS = 1;
	private static final int MAX_PANELS = 15;
	
	private CellScore[] names;
	private IKniffelController controller;
	private IPlayer player;
	private Cell playerName = null;
	
	public PlayerPanel(IKniffelController controller, IPlayer player) {
		this.controller = controller;
		setLayout(new GridLayout(MAX_ROWS,MAX_COLS));
		this.player = player;
		initializeNames();
		setVisible(true);
		repaint();
	}
	
	private void initializeNames() {
		removeAll();
		names = new CellScore[MAX_PANELS];
		if(controller.getPlayer().equals(this.player)) {
			playerName = new Cell("-" + player.getPlayerName() + "-");
		} else {
			playerName = new Cell(player.getPlayerName());
		}
		
		this.add(playerName);
		for (int scoreNumber = 1; scoreNumber < MAX_PANELS; scoreNumber++) {
			names[scoreNumber] = new CellScore(player, scoreNumber);
			this.add(names[scoreNumber]);
			if(scoreNumber == BONUS) {
				this.add(new CellScore(player, TOP_SUM));
			}
			if(scoreNumber == CHANCE) {
				this.add(new CellScore(player, BOTTOM_SUM));
				this.add(new CellScore(player, TOTAL_SUM));
			}
		}
	}
	
	@Override
	public void paint(Graphics g) {
		super.paint(g);
		initializeNames();
	}
}
