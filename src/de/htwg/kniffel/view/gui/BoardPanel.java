package de.htwg.kniffel.view.gui;

import java.awt.GridLayout;

import javax.swing.JPanel;

import de.htwg.kniffel.controller.IKniffelController;

public class BoardPanel extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8837535751812101060L;
	private IKniffelController controller;
	private ScoreNamePanel scoreNamePanel;
	
	public BoardPanel(IKniffelController controller) {
		setLayout(new GridLayout(1,controller.getPlayers().length));
		this.controller = controller;
		scoreNamePanel = new ScoreNamePanel(controller);
		this.add(scoreNamePanel);
		initialzePlayerColumnPanel();
		setVisible(true);
		repaint();
	}
	
	
	private void initialzePlayerColumnPanel() {
		PlayerPanel[] playerPanel  = new PlayerPanel[controller.getPlayers().length];
		for (int i = 0; i < playerPanel.length; i++) {
			playerPanel[i] = new PlayerPanel(controller, controller.getPlayers()[i]);
			this.add(playerPanel[i]);
		}
	}
	
}
