package de.htwg.kniffel.view.gui;

import java.awt.Graphics;
import java.awt.GridLayout;

import javax.swing.BorderFactory;
import javax.swing.JPanel;

import de.htwg.kniffel.controller.IKniffelController;
import de.htwg.kniffel.util.YahtzeeConstants;

public class DiceButtonPanel extends JPanel implements YahtzeeConstants{

	private static final int MAX_ROWS = 1;
	private static final int MAX_COLS = 5;
	private static final long serialVersionUID = 1L;
	private DiceButton[] diceButton;
	private IKniffelController controller;
	
	public DiceButtonPanel(IKniffelController controller) {
		this.controller = controller;
		diceButton = new DiceButton[NUMBER_DICE];
		setLayout(new GridLayout(MAX_ROWS, MAX_COLS));
		setBorder(BorderFactory.createEmptyBorder());
		
		initializeDiceButton();
	}
	
	private void initializeDiceButton() {
		for (int i = 0; i < diceButton.length; i++) {
			diceButton[i] = new DiceButton(controller, i);
			this.add(diceButton[i]);
		}
	}
	
	
	@Override
	public void paint(Graphics g) {
		super.paint(g);
		for (DiceButton button : diceButton) {
			button.setIcons();
		}
	}
	
}
