package de.htwg.kniffel.view.gui;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JTextField;

import de.htwg.kniffel.controller.IKniffelController;
import de.htwg.kniffel.observer.IObserver;

public class KniffelFrame extends JFrame implements IObserver {


	private static final int DEFAULT_X = 500;
	private static final int DEFAULT_Y = 500;
	/**
	 * 
	 */
	private static final long serialVersionUID = 7793547415505965980L;
	private IKniffelController controller;
	private JMenuBar menuBar;
	private BoardPanel boardPanel;
	private DicePanel dicePanel;
	private Container pane;
	
	public KniffelFrame(IKniffelController controller) {
		this.controller = controller;
		this.controller.addObserver(this);
		buildMenuBar();
		buildStartFrame();
	}
	
	@Override
	public void update() {
		pane.remove(boardPanel);
		pane.remove(dicePanel);
		buildFrame();
		repaint();
	}

	private void buildStartFrame() {
		final Container pane;
		setTitle("Kniffel");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(DEFAULT_X, DEFAULT_Y);
		pane = getContentPane();
		pane.setLayout(new BorderLayout());
		pane.add(menuBar);	
		
		final JTextField player1 = new JTextField();
		final JTextField player2 = new JTextField();
		final JTextField player3 = new JTextField();
		final JTextField player4 = new JTextField();
		this.setLayout(new GridLayout(5,2));
		this.add(new JLabel("Player 1"));
		this.add(player1);
		this.add(new JLabel("Player 2"));
		this.add(player2);
		this.add(new JLabel("Player 3"));
		this.add(player3);
		this.add(new JLabel("Player 4"));
		this.add(player4);
		
		
		JButton startGame = new JButton("Start Game");
		startGame.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				if(!player1.getText().isEmpty()) {
					controller.addPlayer(player1.getText());
				}
				if(!player2.getText().isEmpty()) {
					controller.addPlayer(player2.getText());
				}
				if(!player3.getText().isEmpty()) {
					controller.addPlayer(player3.getText());
				}
				if(!player4.getText().isEmpty()) {
					controller.addPlayer(player4.getText());
				}
				controller.initializePlayerNames();
				pane.removeAll();
				buildFrame();
			}
		});
		
		this.add(startGame);
		

		setJMenuBar(menuBar);
		setVisible(true);
		repaint();
	}
	/**
	 * Builds the complete Frame of Kniffel
	 */
	private void buildFrame() {
		boardPanel = new BoardPanel(this.controller);
		dicePanel = new DicePanel(this.controller);
		setTitle("Kniffel");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(DEFAULT_X, DEFAULT_Y);
		pane = getContentPane();
		pane.setLayout(new BorderLayout());
		
		
		pane.add(menuBar);
		pane.add(boardPanel);
		pane.add(dicePanel, BorderLayout.PAGE_END);
		setJMenuBar(menuBar);
		setVisible(true);
		repaint();
	}
	
	private void buildMenuBar() {
		menuBar = new JMenuBar();
		JMenu fileMenu;

		JMenuItem closeMenuItem;
		fileMenu = new JMenu("File");
		

		closeMenuItem = new JMenuItem("Close");
		closeMenuItem.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				System.exit(0);
			}
		});
		fileMenu.add(closeMenuItem);
	
		menuBar.add(fileMenu);
	}
	
}
