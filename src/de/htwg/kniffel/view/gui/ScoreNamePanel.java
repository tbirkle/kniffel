package de.htwg.kniffel.view.gui;

import java.awt.Graphics;
import java.awt.GridLayout;
import javax.swing.JPanel;
import de.htwg.kniffel.controller.IKniffelController;
import de.htwg.kniffel.util.KniffelTextConstants;
import de.htwg.kniffel.util.YahtzeeConstants;

public class ScoreNamePanel extends JPanel implements YahtzeeConstants{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6087480947260697071L;
	private static final int MAX_ROWS = 18;
	private static final int MAX_COLS = 1;
	private static final int MAX_PANELS = 15;
	private static final int MAX_BUTTONS = 15;
	private IKniffelController controller;
	private Cell[] names;
	private SetButton[] buttons;
	
	public ScoreNamePanel(IKniffelController controller) {
		this.controller = controller;
		setLayout(new GridLayout(MAX_ROWS,MAX_COLS));
		checkDiceTry();
		setVisible(true);
		repaint();
	}
	
	private void initializeNames() {
		this.removeAll();
		names = new Cell[MAX_PANELS];
		names[0] = new Cell("");
		this.add(names[0]);
		for (int scoreNumber = 1; scoreNumber < names.length; scoreNumber++) {
			names[scoreNumber] = new Cell(KniffelTextConstants.getText(scoreNumber));
			this.add(names[scoreNumber]);
			if(scoreNumber == BONUS) {
				this.add(new Cell(KniffelTextConstants.getText(TOP_SUM)));
			}
			if(scoreNumber == CHANCE) {
				this.add(new Cell(KniffelTextConstants.getText(BOTTOM_SUM)));
				this.add(new Cell(KniffelTextConstants.getText(TOTAL_SUM)));
			}
		}
	}
	
	private void initializeButtons() {
		this.removeAll();
		buttons = new SetButton[MAX_BUTTONS];
		this.add(new Cell(""));
		for (int scoreNumber = 1; scoreNumber < buttons.length; scoreNumber++) {
			if(scoreNumber != BONUS) {
				buttons[scoreNumber] = new SetButton(controller, scoreNumber);
				this.add(buttons[scoreNumber]);
			}
			if(scoreNumber == BONUS) {
				this.add(new Cell(KniffelTextConstants.getText(BONUS)));
				this.add(new Cell(KniffelTextConstants.getText(TOP_SUM)));
			}
			if(scoreNumber == CHANCE) {
				this.add(new Cell(KniffelTextConstants.getText(BOTTOM_SUM)));
				this.add(new Cell(KniffelTextConstants.getText(TOTAL_SUM)));
			}
		}
	}
	
	private void checkDiceTry() {
		if(controller.getDiceTry() == 0) {
			initializeNames();
		}
		if(controller.getDiceTry() > 0 && controller.getDiceTry() < MAX_TRYS) { 
			initializeButtons();
		}
	}
	
	@Override
	public void paint(Graphics g) {
		super.paint(g);
		checkDiceTry();
	}
}
