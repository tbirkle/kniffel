package de.htwg.kniffel.view.gui;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JButton;
import javax.swing.JOptionPane;

import de.htwg.kniffel.controller.IKniffelController;
import de.htwg.kniffel.util.KniffelTextConstants;

public class SetButton extends JButton {
	
	private static final long serialVersionUID = 1L;

	public SetButton(final IKniffelController controller, final int scoreNumber) {
		this.setText(KniffelTextConstants.getText(scoreNumber));
		this.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				controller.setScore(scoreNumber);
				if(controller.checkFinishAllPlayers()) {
					checkWinner(controller);
				}
			}
		});
	}
	
	private void checkWinner(IKniffelController controller) {
		JOptionPane.showMessageDialog(this, controller.getWinner().getPlayerName() + " won the game");
	}
}
