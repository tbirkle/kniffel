package de.htwg.kniffel.view.gui;

import java.awt.Graphics;
import java.awt.GridLayout;

import javax.swing.BorderFactory;
import javax.swing.JLabel;

import de.htwg.kniffel.model.IPlayer;
import de.htwg.kniffel.model.IScore;
import de.htwg.kniffel.util.YahtzeeConstants;

public class CellScore extends JLabel implements YahtzeeConstants{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8946498401194327144L;
	private IPlayer player = null;
	private int scoreNumber = 0;
	
	public CellScore(IPlayer player, int scoreNumber) {
		this.player = player;
		this.scoreNumber = scoreNumber;
		this.setLayout(new GridLayout(1,1));
		setBorder(BorderFactory.createLoweredBevelBorder());
		setValue();
		setVisible(true);
	}
	
	private void setValue() {
		String value = "";
		IScore score = this.player.getScore();
		if(this.scoreNumber == TOP_SUM) {
			value = String.valueOf(score.getTopScore());
		} else if(this.scoreNumber == BOTTOM_SUM) {
			value = String.valueOf(score.getBottomScore());
		} else if(this.scoreNumber == TOTAL_SUM) {
			value = String.valueOf(score.getPlayerScore());
		} else {
			value = String.valueOf(score.getScore(this.scoreNumber));
		}
		setText(value);
	}
	
	@Override
	public void paint(Graphics g) {
		super.paint(g);
		setValue();
	}
}
