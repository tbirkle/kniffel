package de.htwg.kniffel.view.gui;

import java.awt.Graphics;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JButton;

import de.htwg.kniffel.controller.IKniffelController;

import de.htwg.kniffel.util.YahtzeeConstants;

public class ThrowDiceButton extends JButton implements YahtzeeConstants {

	private static final long serialVersionUID = 1L;
	private IKniffelController controller;
	
	public ThrowDiceButton(final IKniffelController controller, final DiceButtonPanel diceButtonPanel) {
		this.controller = controller;
		setButtonText();
		this.addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				if(controller.getDiceTry() < 3) {
					controller.roll();
					setButtonText();
					diceButtonPanel.repaint();
				}
				
			}
		});
	}
	
	private void setButtonText() {
		if(controller.getDiceTry() < 3) {
			this.setText("W�rfeln Versuch " + (controller.getDiceTry()+1));
		} else {
			this.setText("W�rfeln nicht mehr m�glich");
		}
	}
	
	@Override
	public void paint(Graphics g) {
		super.paint(g);
		setButtonText();
	}
}
