package de.htwg.kniffel.view.tui;


import java.util.Scanner;
import org.apache.log4j.Logger;
import de.htwg.kniffel.controller.IKniffelController;
import de.htwg.kniffel.model.IDice;
import de.htwg.kniffel.observer.IObserver;
import de.htwg.kniffel.util.YahtzeeConstants;

public class TextUI implements IObserver, YahtzeeConstants{
	
	
	private Scanner scanner;
	private IKniffelController controller;
	private static final int MINIMUM_NAME_LENGTH = 4;
	
	private Logger logger = Logger.getLogger("de.htwg.kniffel.view.tui");
	
	public TextUI(IKniffelController controller) {
		scanner = new Scanner(System.in);
		this.controller = controller;
		this.controller.addObserver(this);
		println("| w - roll | s - save dice | u - update scoreboard | a - show saved dice | r - remove saved dice | v - set dice | q - quit |\n");
	}
	
	
	/**
	 * Constructor for testing
	 * @param controller
	 * @param test
	 */
	public TextUI(IKniffelController controller, boolean test) {
		scanner = new Scanner(System.in);
		this.controller = controller;
		this.controller.addObserver(this);
		if(test) {
			println("TestMode");
		}
	}
	
	@Override
	public void update() {
		println(drawTUI());
	}
	
	/**
	 * input playernames
	 */
	public final void inputPlayerNames() {
		println("Bitte Spielernamen eingeben - Eingabe beenden mit (b):");
		String line = scanner.next();
		while(!(line.equals("b")) && line.length() >= MINIMUM_NAME_LENGTH) {
			controller.addPlayer(line);
			println(line);
			line = scanner.next();
		}
		controller.initializePlayerNames();
	}
	
	/**
	 * prints out the result after a roll
	 * @return 
	 */
	public String drawDiceResult(boolean isSaved) {
		StringBuilder sb = new StringBuilder();
		IDice[] dice = controller.getDice();
		
		if(!isSaved) {
			sb.append("Versuch ").append(controller.getDiceTry()).append(":\n");
		}
		else {
			sb.append("Saved Dice").append("Versuch ").append(controller.getDiceTry()).append(":\n");
		}
		
		for (int i = 0; i < dice.length; i++) {
			if(isSaved && dice[i].getIsSaved()) {
				sb.append("W�rfel ").append(i).append(": ").append(dice[i].getResult());
				sb.append("\n");
			}
			if(!isSaved &&!dice[i].getIsSaved()) {
				sb.append("W�rfel ").append(i).append(": ").append(dice[i].getResult());
				sb.append("\n");
			}
		}
		
		
		return sb.toString();
	}
	
	/**
	 * prints the complete Scoreboard
	 */
	public final String drawTUI() {
		StringBuilder sb = new StringBuilder();
		sb.append(controller.drawScoreboard());
		sb.append("| w - roll | s - save dice | u - update scoreboard | a - show saved dice | r - remove saved dice | v - set dice | q - quit |\n");
		sb.append("Player: ");
		sb.append(controller.getPlayer().getPlayerName());
		return sb.toString();
	}
	/**
	 * iterates the inputs of the user
	 * @return returns true if the user entered q 
	 */
	public boolean iterate() {
		return play(scanner.next());
	}
	
	/**
	 * 
	 * @param line is the input of the user
	 * @return returns true if the user entered q to quit the game
	 */
	public boolean play(String line) {
		boolean quit = false;
		
		/**
		 * quit the game
		 */
		if(line.equals("q")) {
			quit = true;
			println("Programm beendet");
		}
		
		/**
		 * roll the dice
		 */
		if(line.equals("w")) {
			int diceTry = controller.roll();
			println(drawDiceResult(false));
			if(diceTry >= MAX_TRYS) {
				println("W�rfeln nicht mehr m�glich");
			}
		}
		
		/**
		 * save the dice
		 */
		if(line.equals("s")) {
			println("W�rfel Nummern eingeben: (BSP: 0 1 2 3 4)");
			controller.saveDice(scanner.next());
		}
		
		/**
		 * show the saved dice
		 */
		if(line.equals("a")) {
			println(drawDiceResult(true));
		}
		
		/**
		 * remove a saved dice
		 */
		if(line.equals("r")) {
			println("W�rfel Nummern eingeben: (BSP: 0 1 2 3 4)");
			controller.removeDice(scanner.next());
		}
		
		/**
		 * update the scoreboard
		 */
		if(line.equals("u")) {
			println(controller.drawScoreboard());
		}
		
		/**
		 * set a score
		 */
		if(line.equals("v")) {
			inputV();
		}
		
		return quit;	
	}

	/**
	 * inputV sets a score
	 */
	private void inputV() {
		boolean isSet = false;
		int scoreNumber = scanner.nextInt();
		isSet = controller.setScore(scoreNumber);
		if(!isSet) {
			println(scoreNumber + " is already set");
		}
		
		if(controller.checkFinishAllPlayers()) {
			println(controller.getWinner().getPlayerName() + "won the game");
		}
	}
	
	private void println(String string) {
		logger.info(string + "\n");
	}
}
