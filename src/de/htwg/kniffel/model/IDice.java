package de.htwg.kniffel.model;

public interface IDice {

	/**
	 * generates a random number between 1 and the maximum number of eyes
	 */
	void rollDice();

	/**
	 * returns the number of eyes of the dice
	 * @return int
	 */
	int getEyes();

	/**
	 * returns the result which is generated after rollDice()
	 * @return int
	 */
	int getResult();

	/**
	 * sets the result of the dice to test
	 * @param result
	 */
	void setResult(int result);

	
	/**
	 * 
	 * @return true if the dice is saved
	 */
	boolean getIsSaved();
	
	/**
	 * Saves the dice
	 * @param isSaved
	 */
	void setIsSaved(boolean isSaved);

}