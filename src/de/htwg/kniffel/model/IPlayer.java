package de.htwg.kniffel.model;

public interface IPlayer {

	/**
	 * returns true if the player has finished
	 */
	boolean getFinished();

	/**
	 * sets finished if the player has finished
	 * @param finished true if the player has finished
	 */
	void setFinished(boolean finished);

	/**
	 * Returns the name of the Player
	 * @return String
	 */
	String getPlayerName();

	/**
	 * Returns the number of the player
	 * @return int
	 */
	int getPlayerNumber();

	/**
	 * Returns the score of the player
	 * @return Score
	 */
	IScore getScore();

	/**
	 * 
	 * @param scoreNumber specifies the name of the score
	 * @param score specifies the value of the score
	 * @return returns true if the score is set
	 */
	boolean setScore(int scoreNumber, int score);

	/**
	 * checks if the player has finished
	 * @return true if the player has finished
	 */
	boolean checkFinish();

}