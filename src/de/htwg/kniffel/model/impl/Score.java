package de.htwg.kniffel.model.impl;

import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import de.htwg.kniffel.model.IScore;
import de.htwg.kniffel.util.YahtzeeConstants;


public class Score implements YahtzeeConstants, IScore {
	
	/**
	 * playerScore is the complete score of the Player
	 */
	private int playerScore;
	
	/**
	 * topScore is the score on the top of the scoreboard
	 */
	private int topScore;
	
	/**
	 * bottomScore is the score on the bottom of the scoreboard
	 */
	private int bottomScore;
	
	/**
	 * score is a map with all scores of the player
	 */
	private Map<Integer, Points> score;
	
	private Integer[] scores = {ONES, TWOS, THREES, FOURS, FIVES, SIX, BONUS, THREE_OF_A_KIND,
			FOUR_OF_A_KIND, SMALL_STRAIGHT, FULL_HOUSE, LARGE_STRAIGHT, YAHTZEE, CHANCE};
	
	public Score() {
		score = new TreeMap<Integer, Points>();
		initialzeScore();		
	}
	
	private static class Points {
		private int points;
		private boolean isSet;
		
		Points(int points, boolean isSet) {
			this.points = points;
			this.isSet = isSet;
		}
	}
	
	
	/**
	 * initializes the scores with 0
	 */
	private void initialzeScore() {
		for (Integer name : scores) {
			score.put(name, new Points(0, false));
		}
		
		this.playerScore = 0;
		this.topScore = 0;
		this.bottomScore = 0;
	}
	
	/**
	 * calculates the top score
	 * @return the top score
	 */
	private int calculateTopScore() {
		int retTopScore = 0;
		calculateBonusScore();
		retTopScore = score.get(ONES).points + score.get(TWOS).points + score.get(THREES).points + score.get(FOURS).points +
					  score.get(FIVES).points + score.get(SIX).points + score.get(BONUS).points;
		
		return retTopScore;
	}
	
	/**
	 * calculates the bottom score
	 * @return the bottom score
	 */
	private int calculateBottomScore() {
		int retBottomScore = 0;
		
		retBottomScore = score.get(THREE_OF_A_KIND).points + score.get(FOUR_OF_A_KIND).points + score.get(FULL_HOUSE).points +
				score.get(SMALL_STRAIGHT).points + score.get(LARGE_STRAIGHT).points + score.get(YAHTZEE).points +
				score.get(CHANCE).points;
		
		return retBottomScore;
	}
	
	/**
	 * checks and calculates the bonus score
	 * @return true if the bonus score is set
	 */
	private boolean calculateBonusScore() {
		int retbonusScore = 0;
		
		for(int i = 1; i < BONUS; i++) {
			if(!score.get(i).isSet) {
				return false;
			}
		}
		retbonusScore  = score.get(ONES).points + score.get(TWOS).points + score.get(THREES).points + score.get(FOURS).points +
		  score.get(FIVES).points + score.get(SIX).points;
		if (retbonusScore >= BONUS_LIMIT) {
			setScoreValue(BONUS, BONUS_SCORE);
			return true;
		} else {
			setScoreValue(BONUS, 0);
			return false;
		}
	}
	
	/* (non-Javadoc)
	 * @see de.htwg.kniffel.model.IScore#getScore(int)
	 */
	@Override
	public int getScore(int scoreNumber) {
		return this.score.get(scoreNumber).points;
	}
	
	/* (non-Javadoc)
	 * @see de.htwg.kniffel.model.IScore#setScoreValue(int, int)
	 */
	@Override
	public boolean setScoreValue(int scoreNumber, int score) {
		if(this.score.get(scoreNumber).isSet) {
			return false;
		}
		this.score.put(scoreNumber, new Points(score, true));
		setTopScore();
		setBottomScore();
		setPlayerScore();
		return true;
	}
	
	/* (non-Javadoc)
	 * @see de.htwg.kniffel.model.IScore#getPlayerScore()
	 */
	@Override
	public int getPlayerScore() {
		return this.playerScore;
	}
	/* (non-Javadoc)
	 * @see de.htwg.kniffel.model.IScore#setPlayerScore()
	 */
	@Override
	public void setPlayerScore() {
		this.playerScore = calculateBottomScore() + calculateTopScore();
	}
	/* (non-Javadoc)
	 * @see de.htwg.kniffel.model.IScore#setTopScore()
	 */
	@Override
	public void setTopScore() {
		this.topScore = calculateTopScore();
	}
	
	/* (non-Javadoc)
	 * @see de.htwg.kniffel.model.IScore#getTopScore()
	 */
	@Override
	public int getTopScore() {
		return this.topScore;
	}
	
	/* (non-Javadoc)
	 * @see de.htwg.kniffel.model.IScore#setBottomScore()
	 */
	@Override
	public void setBottomScore() {
		this.bottomScore = calculateBottomScore();
	}
	
	/* (non-Javadoc)
	 * @see de.htwg.kniffel.model.IScore#getBottomScore()
	 */
	@Override
	public int getBottomScore() {
		return this.bottomScore;
	}
	
	/* (non-Javadoc)
	 * @see de.htwg.kniffel.model.IScore#getBonusScore()
	 */
	@Override
	public int getBonusScore() {
		return score.get(BONUS).points;
	}
	
	/* (non-Javadoc)
	 * @see de.htwg.kniffel.model.IScore#checkFinish()
	 */
	@Override
	public boolean checkFinish() {
		for (Entry<Integer, Points> entry : score.entrySet()) {
			if(!entry.getValue().isSet) {
				return false;
			}
		}
		return true;
	}

}
