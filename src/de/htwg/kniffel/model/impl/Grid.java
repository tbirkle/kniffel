package de.htwg.kniffel.model.impl;

import java.util.LinkedList;
import java.util.List;

import de.htwg.kniffel.model.IGrid;
import de.htwg.kniffel.model.IPlayer;
import de.htwg.kniffel.util.YahtzeeConstants;




public class Grid implements YahtzeeConstants, IGrid{
	
	private static final int MAX_LENGTH = 20;
	
	/**
	 * the name of the grid
	 */
	private String gridName;
	
	/**
	 * an array with all players on the grid
	 */
	private IPlayer[] players;
	
	/**
	 * a list of the names on the top of the grid
	 */
	private List<String> topScoreNameList = new LinkedList<String>();
	
	/**
	 * a list of the names on the bottom of the grid
	 */
	private List<String> bottomScoreNameList = new LinkedList<String>();
	
	/**
	 * a list of the names of the results of the grid
	 */
	private List<String> boardNameList = new LinkedList<String>();
	
	public Grid(String gridName, IPlayer[] players) {
		this.gridName = gridName;
		this.players = players;
		initializeTopScoreNameList();
		initializeBottomScoreNameList();
		initializeBoardNameList();
	}
	
	/* (non-Javadoc)
	 * @see de.htwg.kniffel.model.IGrid#getPlayers()
	 */
	@Override
	public IPlayer[] getPlayers() {
		return players;
	}

	/* (non-Javadoc)
	 * @see de.htwg.kniffel.model.IGrid#getGridName()
	 */
	@Override
	public String getGridName() {
		return gridName;
	}

	
	/**
	 * initializes the names of top the scores for the scoreboard
	 */
	private void initializeTopScoreNameList() {
		String[] topScoreNames = {"ones", "twos", "threes", "fours", "fives",
				"six", "bonus"};

		for (int i = 0; i < topScoreNames.length; i++) {
			this.topScoreNameList.add(topScoreNames[i]);
		}
		
	}
	
	/**
	 * initializes the names of the bottom scores for the scoreboard
	 */
	private void initializeBottomScoreNameList() {
		String[] bottomScoreNames = {"three of a kind", "four of a kind",
				"full house", "small straight", "large straight", "yahtzee", 
				"chance"};
		
		for (int i = 0; i < bottomScoreNames.length; i++) {
			this.bottomScoreNameList.add(bottomScoreNames[i]);
		}
	}
	
	/**
	 * initializes the names of the results for the scoreboard
	 */
	private void initializeBoardNameList() {
		String[] board = {"top sum", "bottom sum", "total sum"};
		
		for (int i = 0; i < board.length; i++) {
			this.boardNameList.add(board[i]);
		}
		
	}
	
	
	/**
	 * draws a single line of the scoreboard
	 * @return String
	 */
	private String drawLine() {
		StringBuilder sb = new StringBuilder();
		sb.append("+");
		for(int i = 0; i < MAX_LENGTH; i++) {
			sb.append("-");
		}
		sb.append("+");
		
		for(int i = 0; i < players.length; i++) {
			for (int j = 0; j < players[i].getPlayerName().length(); j++) {
				sb.append("-");
			}
			sb.append("+");
		}
		sb.append("\n");
		return sb.toString();
	}
	
	/**
	 * draws the playerName of the Scoreboard
	 * @return String
	 */
	private String drawPlayerNames() {
		StringBuilder sb = new StringBuilder();
		sb.append(drawLine());
		sb.append("|");
		for(int i = 0; i < MAX_LENGTH; i++) {
			sb.append(" ");
		}
		
		for(IPlayer player: players) {
			sb.append("|");
			sb.append("").append(player.getPlayerName()).append("");
		}
		sb.append("|\n");
		sb.append(drawLine());
		return sb.toString();
	}
	
	/**
	 * draws the names of the scores
	 * @param list specifies the list which should be printed
	 * @param start 
	 * @return String
	 */
	private String drawList(List<String> list, int start) {
		StringBuilder sb = new StringBuilder();
		
		int scoreName = start;
		for (String name : list) {
			String tmp = "";
			sb.append("|");
			if(scoreName == BONUS) {
				sb.append(name);
				for(int i = 0; i < MAX_LENGTH - name.length() - tmp.length(); i++) {
					sb.append(" ");
				}
			} else {
				sb.append(scoreName).append(". ").append(name);
				tmp = String.valueOf(scoreName);
				for(int i = 0; i < MAX_LENGTH - name.length() - tmp.length() - 2; i++) {
					sb.append(" ");
				}
			}
			sb.append("|");
			for(IPlayer player : players) {
				int score = player.getScore().getScore(scoreName);
				String scoreString = String.valueOf(score); 
				sb.append(scoreString);
				for(int i = 0; i < player.getPlayerName().length() - scoreString.length(); i++) {
					sb.append(" ");
				}
				sb.append("|");
			}
			scoreName++;
			sb.append("\n");
			sb.append(drawLine());
		}
		
		
		return sb.toString();
	}
	/**
	 * draws the names of the results of the scoreboard
	 * @param number
	 * @return String
	 */
	private String drawResult(int number) {
		StringBuilder sb = new StringBuilder();
		sb.append(drawLine());
		sb.append("|");
		sb.append(boardNameList.get(number));
		for(int i = 0; i < MAX_LENGTH - boardNameList.get(number).length(); i++) {
			sb.append(" ");
		}
		sb.append("|");
		
		for(IPlayer player : players) {
			int score = 1;
			
			if(number == 0) {
				score = player.getScore().getTopScore();
			}
			if(number == 1) {
				score = player.getScore().getBottomScore();
			}
			if(number == 2) {
				score = player.getScore().getPlayerScore();
			}
			
			String scoreString = String.valueOf(score); 
			sb.append(scoreString);
			for(int i = 0; i < player.getPlayerName().length() - scoreString.length(); i++) {
				sb.append(" ");
			}
			sb.append("|");
		}
		sb.append("\n");
		if(number == 0) {
			sb.append(drawLine());
		}
		sb.append(drawLine());
		return sb.toString();
	}
	
	
	/* (non-Javadoc)
	 * @see de.htwg.kniffel.model.IGrid#toString()
	 */
	@Override
	/**
	 * returns a String of the complete Scoreboard
	 * @return String
	 */
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(drawPlayerNames()).append(drawList(topScoreNameList, ONES));
		sb.append(drawResult(0));
		sb.append(drawList(bottomScoreNameList, THREE_OF_A_KIND));
		sb.append(drawResult(1));
		sb.append(drawResult(2));
		return sb.toString();
	}
	
}
