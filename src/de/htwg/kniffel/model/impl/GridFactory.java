package de.htwg.kniffel.model.impl;

import de.htwg.kniffel.model.IGrid;
import de.htwg.kniffel.model.IGridFactory;
import de.htwg.kniffel.model.IPlayer;

public class GridFactory implements IGridFactory {

	@Override
	public IGrid create(String name, IPlayer[] players) {
		return new Grid(name, players);
	}

}
