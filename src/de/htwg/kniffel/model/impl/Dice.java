package de.htwg.kniffel.model.impl;

import de.htwg.kniffel.model.IDice;


public class Dice implements IDice {
	/**
	 * maximum number of eyes of the dice
	 */
	private final int eyes;
	/**
	 * result after a roll
	 */
	private int result;

	private boolean isSaved;
	
	public Dice(int eyes) {
		this.eyes = eyes;
		this.isSaved = false;
	}

	
	
	/* (non-Javadoc)
	 * @see de.htwg.kniffel.model.impl.IDice#rollDice()
	 */
	@Override
	public void rollDice() {
		if(!isSaved) {
			result =  (int) (1 + Math.random() * this.getEyes());
		}
	}
	/* (non-Javadoc)
	 * @see de.htwg.kniffel.model.impl.IDice#getEyes()
	 */
	@Override
	public int getEyes() {
		return eyes;
	}

	/* (non-Javadoc)
	 * @see de.htwg.kniffel.model.impl.IDice#getResult()
	 */
	@Override
	public int getResult() {
		return result;
	}
	
	/* (non-Javadoc)
	 * @see de.htwg.kniffel.model.impl.IDice#setResult(int)
	 */
	@Override
	public void setResult(int result) {
		this.result = result;
	}

	@Override
	public boolean getIsSaved() {
		return this.isSaved;
	}

	@Override
	public void setIsSaved(boolean isSaved) {
		this.isSaved = isSaved;
	}
}
