package de.htwg.kniffel.model.impl;

import de.htwg.kniffel.model.IPlayer;
import de.htwg.kniffel.model.IScore;



public class Player implements IPlayer {
	/**
	 * name of the player
	 */
	private String playerName;
	/**
	 * number of the player
	 */
	private int playerNumber;
	/**
	 * score of the player
	 */
	private IScore score;
	
	/**
	 * true if the player has finished the game
	 */
	private boolean finished;
	
	
	public Player(String playerName, int playerNumber) {
		this.playerName = playerName;
		this.playerNumber = playerNumber;
		finished = false;
		this.score = new Score();
	}
	
	/* (non-Javadoc)
	 * @see de.htwg.kniffel.model.IPlayer#getFinished()
	 */
	@Override
	public boolean getFinished() {
		return finished;
	}
	
	/* (non-Javadoc)
	 * @see de.htwg.kniffel.model.IPlayer#setFinished(boolean)
	 */
	@Override
	public void setFinished(boolean finished) {
		this.finished = finished;
	}

	/* (non-Javadoc)
	 * @see de.htwg.kniffel.model.IPlayer#getPlayerName()
	 */
	@Override
	public String getPlayerName() {
		return playerName;
	}
	/* (non-Javadoc)
	 * @see de.htwg.kniffel.model.IPlayer#getPlayerNumber()
	 */
	@Override
	public int getPlayerNumber() {
		return playerNumber;
	}
	
	/* (non-Javadoc)
	 * @see de.htwg.kniffel.model.IPlayer#getScore()
	 */
	@Override
	public IScore getScore() {
		return score;
	}

	
	/* (non-Javadoc)
	 * @see de.htwg.kniffel.model.IPlayer#setScore(int, int)
	 */
	@Override
	public boolean setScore(int scoreNumber, int score) {
		return this.score.setScoreValue(scoreNumber, score);
	}
	
	/* (non-Javadoc)
	 * @see de.htwg.kniffel.model.IPlayer#checkFinish()
	 */
	@Override
	public boolean checkFinish() {
		return this.score.checkFinish();
	}
	
}
