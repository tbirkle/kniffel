package de.htwg.kniffel.model;

public interface IGrid {

	/**
	 * 
	 * @return an array of the players on the grid
	 */
	IPlayer[] getPlayers();

	/**
	 * 
	 * @return the name of the grid
	 */
	String getGridName();

	/**
	 * returns a String of the complete Scoreboard
	 * @return String
	 */
	String toString();

}