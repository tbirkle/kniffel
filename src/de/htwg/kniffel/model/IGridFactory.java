package de.htwg.kniffel.model;

public interface IGridFactory {
	IGrid create(String name, IPlayer[] players);
}
