package de.htwg.kniffel.model;

public interface IScore {

	/**
	 * returns the score "scoreNumber"
	 * @param scoreNumber
	 * @return int
	 */
	int getScore(int scoreNumber);

	/**
	 * sets the score "score" with the number "scoreNumber"
	 * @param scoreNumber
	 * @param score
	 * @return true if the score has set
	 */
	boolean setScoreValue(int scoreNumber, int score);

	/**
	 * 
	 * @return the complete score of the players
	 */
	int getPlayerScore();

	/**
	 * calculates and sets the playerScore
	 */
	void setPlayerScore();

	/**
	 * calculates and sets the topScore
	 */
	void setTopScore();

	/**
	 * 
	 * @return the topScore
	 */
	int getTopScore();

	/**
	 * calculates and sets the bottomScore
	 */
	void setBottomScore();

	/**
	 * 
	 * @return the bottomScore
	 */
	int getBottomScore();

	/**
	 * 
	 * @return the Bonus Score
	 */
	int getBonusScore();

	/**
	 * checks if the player has finished
	 * @return true if the player has finished
	 */
	boolean checkFinish();

}