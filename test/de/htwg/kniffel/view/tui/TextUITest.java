package de.htwg.kniffel.view.tui;

import static org.junit.Assert.*;

import java.util.LinkedList;

import org.apache.log4j.PropertyConfigurator;
import org.junit.Before;
import org.junit.Test;

import de.htwg.kniffel.controller.IKniffelController;
import de.htwg.kniffel.controller.impl.KniffelController;
import de.htwg.kniffel.model.IGridFactory;
import de.htwg.kniffel.model.impl.GridFactory;
import de.htwg.kniffel.view.tui.TextUI;


public class TextUITest {
	
	TextUI tui;

	LinkedList<String> playerNames;
	IKniffelController controller;
	IGridFactory gridFactory;
	
	@Before
	public void setUp() throws Exception {
		PropertyConfigurator.configure("log4j.properties");
		gridFactory = new GridFactory();
		controller = new KniffelController(gridFactory);
		controller.addPlayer("Test1");
		controller.initializePlayerNames();
		tui = new TextUI(controller, true);
	}

	@Test
	public void testPlay() {
		assertEquals(true, tui.play("q"));
		assertEquals(false, tui.play("w"));
		assertEquals(false, tui.play("w"));
		assertEquals(false, tui.play("w"));
		assertEquals(false, tui.play("w"));
		assertEquals(false, tui.play("u"));
		//assertEquals(false, de.htwg.kniffel.view.tui.play("s"));
//		assertEquals(false, de.htwg.kniffel.view.tui.play("r"));
		assertEquals(false, tui.play("a"));
//		assertEquals(false, de.htwg.kniffel.view.tui.play("v"));
	}
	
	@Test
	public void testDrawTUI() {
		assertEquals("+--------------------+-----+\n"+
				 "|                    |Test1|\n"+
				 "+--------------------+-----+\n"+
				 "|1. ones             |0    |\n"+
				 "+--------------------+-----+\n"+
				 "|2. twos             |0    |\n"+
				 "+--------------------+-----+\n"+
				 "|3. threes           |0    |\n"+
				 "+--------------------+-----+\n"+
				 "|4. fours            |0    |\n"+
				 "+--------------------+-----+\n"+
				 "|5. fives            |0    |\n"+
				 "+--------------------+-----+\n"+
				 "|6. six              |0    |\n"+
				 "+--------------------+-----+\n"+
				 "|bonus               |0    |\n"+
				 "+--------------------+-----+\n"+
				 "+--------------------+-----+\n"+
				 "|top sum             |0    |\n"+
				 "+--------------------+-----+\n"+
				 "+--------------------+-----+\n"+
				 "|8. three of a kind  |0    |\n"+
				 "+--------------------+-----+\n"+
				 "|9. four of a kind   |0    |\n"+
				 "+--------------------+-----+\n"+
				 "|10. full house      |0    |\n"+
				 "+--------------------+-----+\n"+
				 "|11. small straight  |0    |\n"+
				 "+--------------------+-----+\n"+
				 "|12. large straight  |0    |\n"+
				 "+--------------------+-----+\n"+
				 "|13. yahtzee         |0    |\n"+
				 "+--------------------+-----+\n"+
				 "|14. chance          |0    |\n"+
				 "+--------------------+-----+\n"+
				 "+--------------------+-----+\n"+
				 "|bottom sum          |0    |\n"+
				 "+--------------------+-----+\n"+
				 "+--------------------+-----+\n"+
				 "|total sum           |0    |\n"+
				 "+--------------------+-----+\n"+
				 "| w - roll | s - save dice | u - update scoreboard | a - show saved dice | r - remove saved dice | v - set dice | q - quit |\n"+
				 "Player: Test1", tui.drawTUI());
	}
}
