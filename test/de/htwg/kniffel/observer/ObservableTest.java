package de.htwg.kniffel.observer;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import de.htwg.kniffel.observer.IObservable;
import de.htwg.kniffel.observer.IObserver;
import de.htwg.kniffel.observer.Observable;

public class ObservableTest {
	private boolean ping=false;
	private TestObserver testObserver;
	private IObservable testObservable;
	
	class TestObserver implements IObserver {
		@Override
		public void update() {
			ping=true;
		}
		
	}

	@Before
	public void setUp() throws Exception {
		testObserver = new TestObserver();
		testObservable = new Observable();
		testObservable.addObserver(testObserver);
	}

	@Test
	public void testNotify() {
		assertFalse(ping);
		testObservable.notifyObservers();
		assertTrue(ping);
	}
	
	@Test
	public void testRemove() {
		assertFalse(ping);
		testObservable.removeObserver(testObserver);
		testObservable.notifyObservers();
		assertFalse(ping);
	}
	
	@Test
	public void testRemoveAll() {
		assertFalse(ping);
		testObservable.removeAllObservers();
		testObservable.notifyObservers();
		assertFalse(ping);
	}


}
