package de.htwg.kniffel.model.impl;

import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

import de.htwg.kniffel.model.IPlayer;


public class PlayerTest {

	IPlayer player1;
	
	
	@Before
	public void setUp() throws Exception {
		player1 = new Player("Tobi", 1);
	}
	
	@Test
	public void testGetFinished() {
		assertFalse(player1.getFinished());
		player1.setFinished(true);
		assertTrue(player1.getFinished());
	}

	@Test
	public void testGetPlayerName() {
		assertEquals("Tobi", player1.getPlayerName());
	}

	@Test
	public void testGetPlayerNumber() {
		assertEquals(1, player1.getPlayerNumber());
	}
	
	@Test
	public void testSetScore() {
		assertTrue(player1.setScore(1, 30));
	}
}
