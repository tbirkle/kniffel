package de.htwg.kniffel.model.impl;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import de.htwg.kniffel.model.IGrid;
import de.htwg.kniffel.model.IPlayer;

public class GridTest {

	IGrid grid;
	IPlayer[] players;
	
	@Before
	public void setUp() throws Exception {
		players = new Player[1];
		players[0] = new Player("Test1", 0);
		grid = new Grid("Test", players);
	}
	
	@Test
	public void getGridName() {
		assertEquals("Test", grid.getGridName());
	}
	
	@Test
	public void testToString() {
		assertEquals("+--------------------+-----+\n"+
					 "|                    |Test1|\n"+
					 "+--------------------+-----+\n"+
					 "|1. ones             |0    |\n"+
					 "+--------------------+-----+\n"+
					 "|2. twos             |0    |\n"+
					 "+--------------------+-----+\n"+
					 "|3. threes           |0    |\n"+
					 "+--------------------+-----+\n"+
					 "|4. fours            |0    |\n"+
					 "+--------------------+-----+\n"+
					 "|5. fives            |0    |\n"+
					 "+--------------------+-----+\n"+
					 "|6. six              |0    |\n"+
					 "+--------------------+-----+\n"+
					 "|bonus               |0    |\n"+
					 "+--------------------+-----+\n"+
					 "+--------------------+-----+\n"+
					 "|top sum             |0    |\n"+
					 "+--------------------+-----+\n"+
					 "+--------------------+-----+\n"+
					 "|8. three of a kind  |0    |\n"+
					 "+--------------------+-----+\n"+
					 "|9. four of a kind   |0    |\n"+
					 "+--------------------+-----+\n"+
					 "|10. full house      |0    |\n"+
					 "+--------------------+-----+\n"+
					 "|11. small straight  |0    |\n"+
					 "+--------------------+-----+\n"+
					 "|12. large straight  |0    |\n"+
					 "+--------------------+-----+\n"+
					 "|13. yahtzee         |0    |\n"+
					 "+--------------------+-----+\n"+
					 "|14. chance          |0    |\n"+
					 "+--------------------+-----+\n"+
					 "+--------------------+-----+\n"+
					 "|bottom sum          |0    |\n"+
					 "+--------------------+-----+\n"+
					 "+--------------------+-----+\n"+
					 "|total sum           |0    |\n"+
					 "+--------------------+-----+\n", grid.toString());
	}

}
