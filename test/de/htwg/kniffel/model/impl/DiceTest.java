package de.htwg.kniffel.model.impl;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import de.htwg.kniffel.model.IDice;
import de.htwg.kniffel.model.impl.Dice;

public class DiceTest {

	IDice dice1;
	
	@Before
	public void setUp() throws Exception {
		dice1 = new Dice(6);
	}

	@Test
	public void testGetEyes() {
		assertEquals(6, dice1.getEyes());
	}

	@Test
	public void testRollDice() {
		for (int i = 0; i < 10000; i++) {
			dice1.rollDice();
			assertTrue(dice1.getResult() > 0);
			assertTrue(dice1.getResult() < 7);
		}
		

	}
	
	@Test
	public void testSetResult() {
		dice1.setResult(1);
		assertEquals(1, dice1.getResult());
	}

}
