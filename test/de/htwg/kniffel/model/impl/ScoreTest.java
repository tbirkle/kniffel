package de.htwg.kniffel.model.impl;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import de.htwg.kniffel.model.IScore;
import de.htwg.kniffel.util.YahtzeeConstants;


public class ScoreTest implements YahtzeeConstants {

	IScore score1;
	
	@Before
	public void setUp() throws Exception {
		score1 = new Score();
	}
	
	@Test
	public void testSetScoreValue() {
		assertTrue(score1.setScoreValue(1, 1));
		assertFalse(score1.setScoreValue(1, 1));
	}
	
	@Test
	public void testGetTopScore() {
		score1.setScoreValue(TWOS, 6);
		score1.setScoreValue(ONES, 6);
		score1.setScoreValue(THREES, 18);
		score1.setScoreValue(FOURS, 20);
		score1.setScoreValue(FIVES, 20);
		score1.setScoreValue(SIX, 24);
		score1.setTopScore();
		assertEquals(129, score1.getTopScore());
	}
	
	@Test
	public void testGetBottomScore() {
		score1.setScoreValue(THREE_OF_A_KIND, 20);
		score1.setScoreValue(FOUR_OF_A_KIND, 30);
		score1.setScoreValue(FULL_HOUSE, FULL_HOUSE_SCORE);
		score1.setScoreValue(SMALL_STRAIGHT, SMALL_STRAIGHT_SCORE);
		score1.setScoreValue(LARGE_STRAIGHT, LARGE_STRAIGHT_SCORE);
		score1.setScoreValue(YAHTZEE, YAHTZEE_SCORE);
		score1.setScoreValue(CHANCE, 20);
		score1.setBottomScore();
		assertEquals(215, score1.getBottomScore());
	}
	
	@Test
	public void testGetScore() {
		score1.setScoreValue(FIVES, 20);
		assertEquals(20, score1.getScore(FIVES));
	}
	
	@Test
	public void testCalculateTopScore() {
		score1.setScoreValue(TWOS, 6);
		score1.setScoreValue(ONES, 6);
		score1.setScoreValue(THREES, 18);
		score1.setScoreValue(FOURS, 0);
		score1.setScoreValue(FIVES, 0);
		score1.setScoreValue(SIX, 0);
		score1.setTopScore();
		assertEquals(30, score1.getTopScore());
	}
	
	@Test
	public void testCalculateBottomScore() {
		score1.setScoreValue(THREE_OF_A_KIND, 20);
		score1.setScoreValue(FOUR_OF_A_KIND, 30);
		score1.setScoreValue(FULL_HOUSE, FULL_HOUSE_SCORE);
		score1.setScoreValue(SMALL_STRAIGHT, SMALL_STRAIGHT_SCORE);
		score1.setScoreValue(LARGE_STRAIGHT, LARGE_STRAIGHT_SCORE);
		score1.setScoreValue(YAHTZEE, YAHTZEE_SCORE);
		score1.setScoreValue(CHANCE, 20);
		score1.setBottomScore();
		assertEquals(215, score1.getBottomScore());
	}
	
	@Test
	public void testCalculatePlayerScore() {
		score1.setScoreValue(TWOS, 6);
		score1.setScoreValue(ONES, 6);
		score1.setScoreValue(THREES, 18);
		score1.setScoreValue(FOURS, 20);
		score1.setScoreValue(FIVES, 20);
		score1.setScoreValue(SIX, 24);
		score1.setTopScore();
		
		score1.setScoreValue(THREE_OF_A_KIND, 20);
		score1.setScoreValue(FOUR_OF_A_KIND, 30);
		score1.setScoreValue(FULL_HOUSE, FULL_HOUSE_SCORE);
		score1.setScoreValue(SMALL_STRAIGHT, SMALL_STRAIGHT_SCORE);
		score1.setScoreValue(LARGE_STRAIGHT, LARGE_STRAIGHT_SCORE);
		score1.setScoreValue(YAHTZEE, YAHTZEE_SCORE);
		score1.setScoreValue(CHANCE, 20);
		score1.setBottomScore();
		score1.setPlayerScore();
		assertEquals(344, score1.getPlayerScore());
	}

	@Test
	public void testCalculateBonusScore() {
		score1.setScoreValue(TWOS, 6);
		score1.setScoreValue(ONES, 6);
		score1.setScoreValue(THREES, 18);
		score1.setScoreValue(FOURS, 20);
		score1.setScoreValue(FIVES, 20);
		score1.setScoreValue(SIX, 24);
		assertEquals(35, score1.getBonusScore());
		assertEquals(129, score1.getTopScore());
	}
	
	@Test
	public void testCalculateBonusScoreFalse() {
		score1.setScoreValue(TWOS, 0);
		score1.setScoreValue(ONES, 0);
		score1.setScoreValue(THREES, 0);
		score1.setScoreValue(FOURS, 0);
		score1.setScoreValue(FIVES, 20);
		score1.setScoreValue(SIX, 24);
		assertEquals(0, score1.getBonusScore());
	}
	
	
	@Test
	public void testGetPlayerScore() {
		assertEquals(0, score1.getPlayerScore());
		score1.setScoreValue(TWOS, 6);
		score1.setScoreValue(ONES, 6);
		score1.setScoreValue(THREES, 18);
		score1.setScoreValue(FOURS, 20);
		score1.setScoreValue(FIVES, 20);
		score1.setScoreValue(SIX, 24);
		score1.setPlayerScore();
		assertEquals(129, score1.getPlayerScore());
	}
	
	@Test
	public void testGetBonusScore() {
		assertEquals(0, score1.getBonusScore());
		score1.setScoreValue(BONUS, BONUS_SCORE);
		assertEquals(BONUS_SCORE, score1.getBonusScore());
	}
	
	@Test
	public void testCheckFinishTrue() {
		score1.setScoreValue(TWOS, 6);
		score1.setScoreValue(ONES, 6);
		score1.setScoreValue(THREES, 18);
		score1.setScoreValue(FOURS, 20);
		score1.setScoreValue(FIVES, 20);
		score1.setScoreValue(SIX, 24);
		score1.setScoreValue(THREE_OF_A_KIND, 20);
		score1.setScoreValue(FOUR_OF_A_KIND, 30);
		score1.setScoreValue(FULL_HOUSE, FULL_HOUSE_SCORE);
		score1.setScoreValue(SMALL_STRAIGHT, SMALL_STRAIGHT_SCORE);
		score1.setScoreValue(LARGE_STRAIGHT, LARGE_STRAIGHT_SCORE);
		score1.setScoreValue(YAHTZEE, YAHTZEE_SCORE);
		score1.setScoreValue(CHANCE, 20);
		score1.setTopScore();
		score1.setTopScore();
		score1.setPlayerScore();
		
		assertTrue(score1.checkFinish());
		
	}
	
	@Test
	public void testCheckFinishFalse() {
		score1.setScoreValue(TWOS, 6);
		score1.setScoreValue(ONES, 6);
		score1.setScoreValue(THREES, 18);
		score1.setScoreValue(FOURS, 20);
		score1.setScoreValue(FIVES, 20);
		score1.setScoreValue(SIX, 24);
		score1.setScoreValue(THREE_OF_A_KIND, 20);
		score1.setScoreValue(FOUR_OF_A_KIND, 30);
		score1.setScoreValue(FULL_HOUSE, FULL_HOUSE_SCORE);
		score1.setScoreValue(SMALL_STRAIGHT, SMALL_STRAIGHT_SCORE);
		score1.setScoreValue(LARGE_STRAIGHT, LARGE_STRAIGHT_SCORE);
		score1.setScoreValue(YAHTZEE, YAHTZEE_SCORE);
		//score1.setScoreValue(CHANCE, 20);
		score1.setTopScore();
		score1.setTopScore();
		score1.setPlayerScore();
		
		assertFalse(score1.checkFinish());
	}
}
