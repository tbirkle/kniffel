package de.htwg.kniffel.controller.logwrapper;

import static org.junit.Assert.*;


import java.util.LinkedList;

import org.apache.log4j.PropertyConfigurator;
import org.junit.Before;
import org.junit.Test;
import de.htwg.kniffel.util.YahtzeeConstants;
import de.htwg.kniffel.model.impl.GridFactory;
import de.htwg.kniffel.controller.logwrapper.KniffelController;


public class KniffelControllerTest implements YahtzeeConstants{
	KniffelController controller;
	LinkedList<String> playerNames;
	GridFactory gridFactory;
	KniffelController controller2;
	@Before
	public void setUp() throws Exception {
		PropertyConfigurator.configure("log4j.properties");
		gridFactory = new GridFactory();
		controller = new KniffelController(gridFactory);
		controller.addPlayer("Test1");
		controller.initializePlayerNames();
		
		controller2 = new KniffelController(gridFactory);
		controller2.addPlayer("Test1");
		controller2.addPlayer("Test2");
		controller2.initializePlayerNames();
	}
	
	
	@Test
	public void testGetPlayers() {
		assertEquals("Test1", controller.getPlayers()[0].getPlayerName());
	}
	
	@Test
	public void testSetScoreFinished() {


		controller2.setTestDice(0, 2);
		controller2.setTestDice(1, 1);
		controller2.setTestDice(2, 3);
		controller2.setTestDice(3, 4);
		controller2.setTestDice(4, 5);
		controller2.setScore(FOUR_OF_A_KIND);
		
		controller2.setTestDice(0, 2);
		controller2.setTestDice(1, 1);
		controller2.setTestDice(2, 3);
		controller2.setTestDice(3, 5);
		controller2.setTestDice(4, 4);
		controller2.setScore(LARGE_STRAIGHT);

		controller2.setTestDice(0, 3);
		controller2.setTestDice(1, 1);
		controller2.setTestDice(2, 4);
		controller2.setTestDice(3, 2);
		controller2.setTestDice(4, 1);
		controller2.setScore(SMALL_STRAIGHT);
		

		
		controller2.setTestDice(0, 1);
		controller2.setTestDice(1, 1);
		controller2.setTestDice(2, 1);
		controller2.setTestDice(3, 1);
		controller2.setTestDice(4, 1);
		controller2.setScore(CHANCE);
		
		
		controller2.setTestDice(0, 1);
		controller2.setTestDice(1, 1);
		controller2.setTestDice(2, 1);
		controller2.setTestDice(3, 2);
		controller2.setTestDice(4, 2);
		controller2.setScore(FULL_HOUSE);
		

		
		controller2.setTestDice(0, 1);
		controller2.setTestDice(1, 1);
		controller2.setTestDice(2, 1);
		controller2.setTestDice(3, 1);
		controller2.setTestDice(4, 1);
		controller2.setScore(ONES);
		

		
		controller2.setTestDice(0, 1);
		controller2.setTestDice(1, 1);
		controller2.setTestDice(2, 1);
		controller2.setTestDice(3, 1);
		controller2.setTestDice(4, 1);
		controller2.setScore(TWOS);
		
		controller2.setTestDice(0, 1);
		controller2.setTestDice(1, 1);
		controller2.setTestDice(2, 1);
		controller2.setTestDice(3, 1);
		controller2.setTestDice(4, 1);
		controller2.setScore(THREES);
		
		controller2.setTestDice(0, 1);
		controller2.setTestDice(1, 1);
		controller2.setTestDice(2, 1);
		controller2.setTestDice(3, 1);
		controller2.setTestDice(4, 1);
		controller2.setScore(FOURS);
		
		controller2.setTestDice(0, 1);
		controller2.setTestDice(1, 1);
		controller2.setTestDice(2, 1);
		controller2.setTestDice(3, 1);
		controller2.setTestDice(4, 1);
		controller2.setScore(FIVES);
		
		controller2.setTestDice(0, 1);
		controller2.setTestDice(1, 1);
		controller2.setTestDice(2, 1);
		controller2.setTestDice(3, 1);
		controller2.setTestDice(4, 1);
		controller2.setScore(SIX);
		
		controller2.setTestDice(0, 1);
		controller2.setTestDice(1, 1);
		controller2.setTestDice(2, 1);
		controller2.setTestDice(3, 2);
		controller2.setTestDice(4, 3);
		controller2.setScore(THREE_OF_A_KIND);
		
		controller2.setTestDice(0, 1);
		controller2.setTestDice(1, 1);
		controller2.setTestDice(2, 1);
		controller2.setTestDice(3, 1);
		controller2.setTestDice(4, 1);
		controller2.setScore(YAHTZEE);
		
		System.out.println("---------" + controller2.getPlayer().getPlayerName());
		assertFalse(controller2.checkFinishAllPlayers());
		
		
		System.out.println(controller2.iteratePlayers());
		controller2.iteratePlayers();
		System.out.println(controller2.getPlayer().getPlayerName());
		
		
		controller2.setTestDice(0, 2);
		controller2.setTestDice(1, 1);
		controller2.setTestDice(2, 3);
		controller2.setTestDice(3, 4);
		controller2.setTestDice(4, 5);
		controller2.setScore(FOUR_OF_A_KIND);

		controller2.setTestDice(0, 2);
		controller2.setTestDice(1, 1);
		controller2.setTestDice(2, 3);
		controller2.setTestDice(3, 5);
		controller2.setTestDice(4, 4);
		controller2.setScore(LARGE_STRAIGHT);
		
		controller2.setTestDice(0, 3);
		controller2.setTestDice(1, 1);
		controller2.setTestDice(2, 4);
		controller2.setTestDice(3, 2);
		controller2.setTestDice(4, 1);

		controller2.setScore(SMALL_STRAIGHT);
		
		controller2.setTestDice(0, 1);
		controller2.setTestDice(1, 1);
		controller2.setTestDice(2, 1);
		controller2.setTestDice(3, 1);
		controller2.setTestDice(4, 1);
		controller2.setScore(CHANCE);
		
		controller2.setTestDice(0, 1);
		controller2.setTestDice(1, 1);
		controller2.setTestDice(2, 1);
		controller2.setTestDice(3, 2);
		controller2.setTestDice(4, 2);
		controller2.setScore(FULL_HOUSE);
		
		controller2.setTestDice(0, 1);
		controller2.setTestDice(1, 1);
		controller2.setTestDice(2, 1);
		controller2.setTestDice(3, 1);
		controller2.setTestDice(4, 1);
		controller2.setScore(ONES);
		
		controller2.setTestDice(0, 1);
		controller2.setTestDice(1, 1);
		controller2.setTestDice(2, 1);
		controller2.setTestDice(3, 1);
		controller2.setTestDice(4, 1);
		controller2.setScore(TWOS);
		
		controller2.setTestDice(0, 1);
		controller2.setTestDice(1, 1);
		controller2.setTestDice(2, 1);
		controller2.setTestDice(3, 1);
		controller2.setTestDice(4, 1);
		controller2.setScore(THREES);
		
		controller2.setTestDice(0, 1);
		controller2.setTestDice(1, 1);
		controller2.setTestDice(2, 1);
		controller2.setTestDice(3, 1);
		controller2.setTestDice(4, 1);
		controller2.setScore(FOURS);
		
		controller2.setTestDice(0, 1);
		controller2.setTestDice(1, 1);
		controller2.setTestDice(2, 1);
		controller2.setTestDice(3, 1);
		controller2.setTestDice(4, 1);
		controller2.setScore(FIVES);
		
		controller2.setTestDice(0, 1);
		controller2.setTestDice(1, 1);
		controller2.setTestDice(2, 1);
		controller2.setTestDice(3, 1);
		controller2.setTestDice(4, 1);
		controller2.setScore(SIX);
		
		controller2.setTestDice(0, 1);
		controller2.setTestDice(1, 1);
		controller2.setTestDice(2, 1);
		controller2.setTestDice(3, 1);
		controller2.setTestDice(4, 1);
		controller2.setScore(THREE_OF_A_KIND);
		
		controller2.setTestDice(0, 1);
		controller2.setTestDice(1, 1);
		controller2.setTestDice(2, 1);
		controller2.setTestDice(3, 1);
		controller2.setTestDice(4, 1);
		controller2.setScore(YAHTZEE);
		
		assertTrue(controller2.checkFinishAllPlayers());
		assertEquals("Test2", controller2.getWinner().getPlayerName());
	}

	@Test
	public void testCheckWinner() {
		controller.setTestDice(0, 1);
		controller.setTestDice(1, 1);
		controller.setTestDice(2, 1);
		controller.setTestDice(3, 1);
		controller.setTestDice(4, 1);
		controller.setScore(YAHTZEE);
		assertEquals(controller.getPlayer(), controller.checkWinner());
	}
	
	@Test
	public void testIteratePlayers() {
		int retVal = controller.iteratePlayers();
		assertEquals(controller.getPlayer().getPlayerNumber(), retVal);
	}
	
	@Test
	public void testCheckStraightLargeCorrect() {
		controller.setTestDice(0, 2);
		controller.setTestDice(1, 1);
		controller.setTestDice(2, 3);
		controller.setTestDice(3, 5);
		controller.setTestDice(4, 4);
		controller.setScore(LARGE_STRAIGHT);
		assertEquals(LARGE_STRAIGHT_SCORE, controller.getScore().getScore(LARGE_STRAIGHT));
	}
	
	@Test
	public void testCheckStraightSmallCorrect() {
		controller.setTestDice(0, 3);
		controller.setTestDice(1, 1);
		controller.setTestDice(2, 4);
		controller.setTestDice(3, 2);
		controller.setTestDice(4, 1);
		controller.setScore(SMALL_STRAIGHT);
		assertEquals(SMALL_STRAIGHT_SCORE, controller.getScore().getScore(SMALL_STRAIGHT));
	}
	
	@Test
	public void testCheckStraightSmallWrong() {
		controller.setTestDice(0, 2);
		controller.setTestDice(1, 1);
		controller.setTestDice(2, 4);
		controller.setTestDice(3, 2);
		controller.setTestDice(4, 1);
		controller.setScore(SMALL_STRAIGHT);
		assertEquals(0, controller.getScore().getScore(SMALL_STRAIGHT));
	}
	
	@Test
	public void testCheckStraightLargeWrong() {
		controller.setTestDice(0, 2);
		controller.setTestDice(1, 1);
		controller.setTestDice(2, 4);
		controller.setTestDice(3, 2);
		controller.setTestDice(4, 1);
		controller.setScore(LARGE_STRAIGHT);
		assertEquals(0, controller.getScore().getScore(LARGE_STRAIGHT));
	}
	
	@Test
	public void testCountResult() {
		controller.setTestDice(0, 1);
		controller.setTestDice(1, 1);
		controller.setTestDice(2, 1);
		controller.setTestDice(3, 1);
		controller.setTestDice(4, 1);
		controller.setScore(CHANCE);
		assertEquals(5, controller.getScore().getScore(CHANCE));
	}
	
	@Test
	public void testCheckFullHouseCorrect() {
		controller.setTestDice(0, 1);
		controller.setTestDice(1, 1);
		controller.setTestDice(2, 1);
		controller.setTestDice(3, 2);
		controller.setTestDice(4, 2);
		controller.setScore(FULL_HOUSE);
		assertEquals(FULL_HOUSE_SCORE, controller.getScore().getScore(FULL_HOUSE));
	}
	
	@Test
	public void testCheckFullHouseWrong() {
		controller.setTestDice(0, 3);
		controller.setTestDice(1, 1);
		controller.setTestDice(2, 1);
		controller.setTestDice(3, 2);
		controller.setTestDice(4, 2);
		controller.setScore(FULL_HOUSE);
		assertEquals(0, controller.getScore().getScore(FULL_HOUSE));
	}
	
	@Test
	public void testCheckFullHouseCorrectInverse() {
		controller.setTestDice(0, 2);
		controller.setTestDice(1, 2);
		controller.setTestDice(2, 2);
		controller.setTestDice(3, 1);
		controller.setTestDice(4, 1);
		controller.setScore(FULL_HOUSE);
		assertEquals(FULL_HOUSE_SCORE, controller.getScore().getScore(FULL_HOUSE));
	}
	
	@Test
	public void testCheckFullHouseWrongInverse() {
		controller.setTestDice(0, 2);
		controller.setTestDice(1, 2);
		controller.setTestDice(2, 2);
		controller.setTestDice(3, 1);
		controller.setTestDice(4, 3);
		controller.setScore(FULL_HOUSE);
		assertEquals(0, controller.getScore().getScore(FULL_HOUSE));
	}
	
	
	@Test
	public void testCheckTopScore() {
		controller.setTestDice(0, 1);
		controller.setTestDice(1, 1);
		controller.setTestDice(2, 1);
		controller.setTestDice(3, 1);
		controller.setTestDice(4, 1);
		controller.setScore(ONES);
		assertEquals(5, controller.getScore().getScore(ONES));
		
		controller.setTestDice(0, 1);
		controller.setTestDice(1, 1);
		controller.setTestDice(2, 1);
		controller.setTestDice(3, 1);
		controller.setTestDice(4, 1);
		controller.setScore(ONES);
		assertEquals(5, controller.getScore().getScore(ONES));
	}
	
	@Test
	public void testCheckEqualDiceYahtzee() {
		controller.setTestDice(0, 1);
		controller.setTestDice(1, 1);
		controller.setTestDice(2, 1);
		controller.setTestDice(3, 1);
		controller.setTestDice(4, 1);
		controller.setScore(YAHTZEE);
		assertEquals(YAHTZEE_SCORE, controller.getScore().getScore(YAHTZEE));
	}
	
	@Test
	public void testCheckEqualDice3Kind() {
		controller.setTestDice(0, 1);
		controller.setTestDice(1, 1);
		controller.setTestDice(2, 1);
		controller.setTestDice(3, 2);
		controller.setTestDice(4, 3);
		controller.setScore(THREE_OF_A_KIND);
		assertEquals(8, controller.getScore().getScore(THREE_OF_A_KIND));
	}
	
	@Test
	public void testCheckEqualDice4Kind() {
		controller.setTestDice(0, 1);
		controller.setTestDice(1, 1);
		controller.setTestDice(2, 1);
		controller.setTestDice(3, 1);
		controller.setTestDice(4, 4);
		controller.setScore(FOUR_OF_A_KIND);
		assertEquals(8, controller.getScore().getScore(FOUR_OF_A_KIND));
	}
	
	@Test
	public void testCheckEqualDiceWrong() {		
		controller.setTestDice(0, 2);
		controller.setTestDice(1, 1);
		controller.setTestDice(2, 3);
		controller.setTestDice(3, 4);
		controller.setTestDice(4, 5);
		
		controller.setScore(FOUR_OF_A_KIND);
		assertEquals(0, controller.getScore().getScore(FOUR_OF_A_KIND));
		
		controller.setTestDice(0, 2);
		controller.setTestDice(1, 1);
		controller.setTestDice(2, 3);
		controller.setTestDice(3, 4);
		controller.setTestDice(4, 5);
		controller.setScore(THREE_OF_A_KIND);
		assertEquals(0, controller.getScore().getScore(THREE_OF_A_KIND));
		
		

		controller.setTestDice(0, 2);
		controller.setTestDice(1, 1);
		controller.setTestDice(2, 3);
		controller.setTestDice(3, 4);
		controller.setTestDice(4, 5);
		controller.setScore(YAHTZEE);
		assertEquals(0, controller.getScore().getScore(YAHTZEE));
	}

	@Test
	public void testSetScore() {
		controller.setTestDice(0, 1);
		controller.saveDice("0");
		assertTrue(controller.setScore(ONES));
		

		controller.setTestDice(0, 1);
		controller.setTestDice(1, 2);
		controller.saveDice("0");
		assertFalse(controller.setScore(ONES));
	}
	

	
	@Test
	public void testRemoveDice() {
		controller.setTestDice(0, 1);
		controller.saveDice("0");
		assertEquals(0, controller.removeDice("0"));
	}
	
	@Test
	public void testDrawScoreboard() {
		assertEquals("+--------------------+-----+\n"+
				 "|                    |Test1|\n"+
				 "+--------------------+-----+\n"+
				 "|1. ones             |0    |\n"+
				 "+--------------------+-----+\n"+
				 "|2. twos             |0    |\n"+
				 "+--------------------+-----+\n"+
				 "|3. threes           |0    |\n"+
				 "+--------------------+-----+\n"+
				 "|4. fours            |0    |\n"+
				 "+--------------------+-----+\n"+
				 "|5. fives            |0    |\n"+
				 "+--------------------+-----+\n"+
				 "|6. six              |0    |\n"+
				 "+--------------------+-----+\n"+
				 "|bonus               |0    |\n"+
				 "+--------------------+-----+\n"+
				 "+--------------------+-----+\n"+
				 "|top sum             |0    |\n"+
				 "+--------------------+-----+\n"+
				 "+--------------------+-----+\n"+
				 "|8. three of a kind  |0    |\n"+
				 "+--------------------+-----+\n"+
				 "|9. four of a kind   |0    |\n"+
				 "+--------------------+-----+\n"+
				 "|10. full house      |0    |\n"+
				 "+--------------------+-----+\n"+
				 "|11. small straight  |0    |\n"+
				 "+--------------------+-----+\n"+
				 "|12. large straight  |0    |\n"+
				 "+--------------------+-----+\n"+
				 "|13. yahtzee         |0    |\n"+
				 "+--------------------+-----+\n"+
				 "|14. chance          |0    |\n"+
				 "+--------------------+-----+\n"+
				 "+--------------------+-----+\n"+
				 "|bottom sum          |0    |\n"+
				 "+--------------------+-----+\n"+
				 "+--------------------+-----+\n"+
				 "|total sum           |0    |\n"+
				 "+--------------------+-----+\n", controller.drawScoreboard());
	}
}
